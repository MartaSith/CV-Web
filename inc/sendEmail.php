﻿<?php
require_once '../lib/PHPMailer/src/PHPMailer.php';
require_once '../lib/PHPMailer/src/SMTP.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$siteOwnersEmail = 'contacto@martagijon.me';

if($_POST) {

    $name = trim(stripslashes($_POST['contactName']));
    $email = trim(stripslashes($_POST['contactEmail']));
    $subject = trim(stripslashes($_POST['contactSubject']));
    $contact_message = trim(stripslashes($_POST['contactMessage']));

    // Check Name
    if (strlen($name) < 2) {
        $error['name'] = "Por favor, inserte su nombre.";
    }
    // Check Email
    if (!preg_match('/^[a-z0-9&\'\.\-_\+]+@[a-z0-9\-]+\.([a-z0-9\-]+\.)*+[a-z]{2}/is', $email)) {
        $error['email'] = "Por favor, inserte un correo electrónico válido.";
    }
    // Check Message
    if (strlen($contact_message) < 15) {
        $error['message'] = "Por favor, introduzca su mensaje. Debe contener al menos 15 caracteres.";
    }
    // Subject
    if ($subject == '') { $subject = "Asunto"; }


    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
    $mail->SMTPDebug = 0;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'mail.adoptaunvenezolano.org';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = $siteOwnersEmail;                 // SMTP username
    $mail->Password = 'marta675';                           // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                                    // TCP port to connect to

    //Recipients
    $mail->setFrom($siteOwnersEmail, $name);
    $mail->addAddress($siteOwnersEmail, 'Marta H. Gijón');     // Add a recipient

    //Content
    $mail->isHTML(false);                                  // Set email format to HTML
    $mail->Subject = $subject;
    $mail->Body    = $name.' ('.$email.') has sent a message: '.$contact_message;

    $mail->send();
    echo "Gracias por su correo. Lo responderé a la mayor brevedad.";
}

?>
